public class Account {
    private String id;
    private String name;
    private int balance = 0;

    public Account(String id, String name){
        this.id = id;
        this.name = name;
    }
    public Account(String id, String name, int balance){
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int credit(int amount){
        this.balance = balance+amount;
        return this.balance;
    }
    public void debit(int amount){
        if (amount<=balance){
            balance-= amount;
        }
        else{
            System.out.println("Amount axceeded balance " + balance);
        }
    }
    public void transferTo(Account account,int amount){
        if (amount<=balance){
            account.balance+= amount;
        }
        else{
            System.out.println("Amount axceeded balance " + account.balance);
        }
    }
    public String getId(){
        return id;
    }
    public  String getName(){
        return name;
    }
    public int getBalance(){
        return balance;
    }
    public String toString(){
        return "Account[id = " + id + ", name = " + name +", balance = "+ balance + " ]";
    }

}
